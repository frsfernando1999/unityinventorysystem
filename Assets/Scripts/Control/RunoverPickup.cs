using System;
using UnityEngine;
using GameDevTV.Inventories;
using InventoryExample.Control;

namespace InventoryExample.Control
{
    [RequireComponent(typeof(Pickup))]
    public class RunOverPickup : MonoBehaviour
    {
        private Pickup _pickup;

        private void Awake()
        {
            _pickup = GetComponent<Pickup>();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                _pickup.PickupItem();
            }
        }
    }
}